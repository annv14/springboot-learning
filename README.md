Hướng dẫn Spring Boot
=========================

![Spring Boot 2.0](https://img.shields.io/badge/Spring%20Boot-2.0-brightgreen.svg)
![Mysql 5.6](https://img.shields.io/badge/Mysql-5.6-blue.svg)
![JDK 1.8](https://img.shields.io/badge/JDK-1.8-brightgreen.svg)
![Maven](https://img.shields.io/badge/Maven-3.5.0-yellowgreen.svg)
 
Spring Boot và mọi thứ liên quan.

## Trong repo này gồm có
### I. Spring Boot Basic 
1. [Hướng dẫn loosely coupled](basic-dependency-loosely-coupled/readme.md)
2. [Hướng dẫn @Component và @Autowired](spring-boot-1-helloworld-@Component-@Autowired/readme.md)
3. [@Autowired - @Primary - @Qualifier](spring-boot-2-helloworld-@Primary - @Qualifier/readme.md)
4. [Spring Bean Life Cycle + @PostConstruct và @PreDestroy](Spring Bean Life Cycle + @PostConstruct và @PreDestroy/readme.md)
5. [@Component vs @Service vs @Repository](spring-boot-4-@Component-@Service-@Repository/readme.md)
6. [Component Scan là gì?](spring-boot-5-Component-Scan/readme.md)
7. [@Configuration và @Bean](spring-boot-6-@configuration-@Bean/readme.md)
8. [Spring Boot Application Config và @Value](spring-boot-7-spring-application-properties-@Value/readme.md)
9. [Tạo Web Helloworld với @Controller](spring-boot-8-@Controller-web-helloworld/readme.md)
10. [Hướng dẫn chi tiết làm Web với Thymeleaf + Demo Full](spring-boot-9-thymeleaf/readme.md)
11. [@RequestMapping + @PostMapping + @ModelAttribute + @RequestParam + Web To-Do với Thymeleaf](spring-boot-10-@PostMapping-@ModelAttribute-thymeleaf/readme.md)
12. [ Hướng dẫn Spring Boot JPA + MySQL](spring-boot-11-JPA-MySQL/readme.md)
13. [Spring JPA Method + @Query](spring-boot-12-jpa-method-and-@Query-@Param/readme.md)
14. [Special」 Chi tiết Spring Boot + Thymeleaf + MySQL + i18n + Web Demo](spring-boot-13-spring-boot-thymeleaf-mysql-web/readme.md)
15. [Restful API + @RestController + @PathVariable + @RequestBody](spring-boot-14-restful-api-@RestController-@RequestBody-@PathVariable/readme.md)
16. [Exception Handling @ExceptionHandler + @RestControllerAdvice / @ControllerAdvice + @ResponseStatus](spring-boot-15-@ControllerAdvice-@RestControllerAdvice-@ExceptionHandler-@ResponseStatus/readme.md)
17. [Hướng dẫn sử dụng @ConfigurationProperties](spring-boot-16-@ConfigurationProperties/readme.md)
18. [Chạy nhiều môi trường với Spring Profile](spring-boot-17-spring-profiles/readme.md)
19. [Hướng dẫn chi tiết Test Spring Boot](spring-boot-18-testing-in-spring-boot/readme.md)
20. [Hướng dẫn chi tiết Test Spring Boot (Phần 2)](spring-boot-19-testing-in-spring-boot-2/readme.md)
21. [Hướng dẫn tạo Bean có điều kiện với @Conditional](spring-boot-@Conditional/readme.md)
22. [Hướng dẫn tự tạo custom @Conditional](spring-boot-@Conditional-2-custom-conditional/readme.md)
23. [Annotation @Lazy trong Spring Boot](spring-boot-@Lazy-Anotation/readme.md)
24. [Hướng dẫn sử dụng Spring Properties](spring-configuration-properties/readme.md)
25. [Hướng dẫn Spring Boot Data + Redis cơ bản](spring-redis/readme.md)
26. [Xử lý sự kiện với @EventListener + @Async](spring-boot-@EventListener-@Async/readme.md)
27. [Các sự kiện của ApplicationContext](spring-boot-application-context-events/readme.md)
28. [RESTful API Document Tạo với Spring Boot + Swagger](spring-boot-swagger2/readme.md)

### II. JPA & Hibernate

1. [Hướng dẫn Auditing với Hibernate + JPA](spring-boot-jpa-auditing/readme.md)
2. [Hướng dẫn sử dụng @OneToOne](jpa-hibernate-one-to-one/readme.md)
3. [Hướng dẫn sử dụng @OneToMany và @ManyToOne](jpa-hibernate-one-to-many/readme.md)
4. [Hướng dẫn sử dụng @ManyToMany](jpa-hibernate-many-to-many/readme.md)
5. [Hướng dẫn Query phân trang bằng Pageable (Phần 1)](jpa-hibernate-pageable/readme.md)
6. [Hướng dẫn sử dụng Criteria API trong Hibernate (Phần 2)](jpa-hibernate-criteria/readme.md)
7. [Spring JPA: Hướng dẫn sử dụng Specification (Phần 1)](jpa-hibernate-specifications/readme.md)
8. [Hướng dẫn tự tạo Validator để kiểm tra Model & Entity](jpa-hibernate-custom-validation/readme.md)

### III. Security

1. [Hướng dẫn Authorization với Spring Security (căn bản)](spring-security-example/readme.md)
2. [Hướng dẫn Spring Boot + Security + H2 Database](spring-security-hibernate/readme.md)
3. [Hướng dẫn String Security + JWT (Json Web Token)](spring-security-hibernate-jwt/readme.md)

### Others

1. [Test-Hướng dẫn toàn tập Mockito](test-mockito-basic/readme.md)


