[Spring Boot] Hướng dẫn tự tạo Validator để kiểm tra Model & Entity
- Giới thiệu
- Cài đặt
- Prepare
- Tạo Annotation
- Tạo Validator
- Chạy thử

# Giới thiệu
Bản thân Hibernate và Java đã cung cấp cho chúng ta rất nhiều các Annotation để validate dữ liệu của model.

Chẳng hạn như: @@NotBlank, @Size, @Email, v.v..

Tuy nhiên, trên thực tế, chúng ta có rất nhiều các điều kiện được đặt ra, tuỳ thuộc vào business và mô hình dự án.

Ví dụ như trong dự án của tôi, tôi muốn tất cả User đều phải có một thuộc tính là lodaId.

Một lodaId hợp lệ là chuỗi String có tiền tố: lgs://xxxx

Lúc này, làm sao để tôi chắc chắn được rằng mọi User trước khi tạo đều phải có lodaId hợp lệ?

Rõ ràng tôi phải tự tạo ra một bộ kiểm tra cho riêng mình để kiểm soát tính hợp lệ của User.

Rất may, hibernate-validator sẽ giúp tôi làm điều đó.

Trong bài có sử dụng các kiến thức:

Spring Boot
jpa
lombok
# Cài đặt
pom.xml
```
https://mvnrepository.com/artifact/org.hibernate/hibernate-validator
<dependency>
	<groupId>org.hibernate</groupId>
	<artifactId>hibernate-validator</artifactId>
</dependency>


# Prepare
Trước khi tạo ra một bộ Validator cho riêng mình, chúng ta tạo ra các thành phần chính.

Tạo ra model User

User.java
```
@Data
public class User {
    private String lodaId;
}
```
Tạo Controller giao tiếp với client.

UserController.java
```
@RestController
@RequestMapping("api/v1/users")
public class UserController {

    /*
        Đánh dấu object với @Valid để validator tự động kiểm tra object đó có hợp lệ hay không
     */
    @PostMapping
    public Object createUser(@Valid @RequestBody User user) {
        return user;
    }

}
```
# Tạo Annotation
Để tạo ra một validator với Hibernate-Validator, chúng ta cần khai báo một annotation mới.

Ở đây, tôi tạo ra một Annotation của chính mình như sau:
```
/**
 * Khai báo một custom annotation
 */
@Documented
@Constraint(validatedBy = LodaIdValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LodaId {
    // trường message là bắt buộc, khai báo nội dung sẽ trả về khi field k hợp lệ
    String message() default "LodaId must start with lgs://";
    // Cái này là bắt buộc phải có để Hibernate Validator có thể hoạt động
    Class<?>[] groups() default {};
    // Cái này là bắt buộc phải có để Hibernate Validator có thể hoạt động
    Class<? extends Payload>[] payload() default {};
}
```
Vậy là xong, giờ chỉ cần gắn @LodaId lên trường nào cần kiểm tra tính hợp lệ.
```
@Data
public class User {
    // Đánh dấu field lodaId sẽ cần validate bởi @LodaId
    @LodaId
    private String lodaId;
}
```
# Tạo Validator
Sau khi đã tạo ra @LodaId, chúng ta cần tạo ra bộ kiểm tra cho Annotation này.

Rất đơn giản, bạn chỉ cần kế thừa lớp ConstraintValidator của Hibernate Validator
```
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LodaIdValidator implements ConstraintValidator<LodaId, String> {
    private static final String LODA_PREFIX = "lgs://";

    lgs     * Kiểm tra tính hợp lệ của trường được đánh dấu bởi @LodaId 
     * @param s
     * @param constraintValidatorContext
     * @return
     */
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null || s.isEmpty()) return false;

        return s.startsWith(LODA_PREFIX);
    }
}
```
# Chạy thử
Request một User hợp lệ:
```
{
	"lodaId": "lgs://user_1"
}
trả về thành công 200.
```
